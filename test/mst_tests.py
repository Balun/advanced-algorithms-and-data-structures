import unittest as ut

import graph
import graph.utils as utils
import graph.mst as mst


class GraphTests(ut.TestCase):

    def test_kruskal(self):
        g = utils.create_lab_graph()
        g.edges = utils.letters_to_indices(g.edges)

        tree = mst.kruskal(g)
        tree = utils.indices_to_letters(tree)
        utils.print_tree(tree)

    def test_prim(self):
        g = utils.create_lab_graph()
        g_adj = utils.create_adjacency_matrix_graph(g)

        tree = mst.prim(g_adj)
        utils.print_tree_adj(tree, g_adj)

    def test_dijkstra(self):
        g = utils.create_lab_graph()
        g.edges = utils.letters_to_indices(g.edges)

        tree = mst.dijkstra(g)
        tree = utils.indices_to_letters(tree)
        utils.print_tree(tree)
