"""

"""

from collections import defaultdict
from string import ascii_lowercase

import sys


class Graph:
    """

    """

    def __init__(self, vertices):
        """

        :param vertices:
        """
        self.V = vertices
        self._graph = []

    @property
    def edges(self):
        """

        :return:
        """
        return self._graph

    @edges.setter
    def edges(self, new_edges):
        """

        :param new_edges:
        :return:
        """
        self._graph = new_edges

    # function to add an edge to graph
    def add_edge(self, u, v, w):
        """

        :param u:
        :param v:
        :param w:
        :return:
        """
        self._graph.append([u, v, w])

    # A utility function to find set of an element i
    # (uses path compression technique)
    def find(self, parent, i):
        """

        :param parent:
        :param i:
        :return:
        """
        letter = ascii_lowercase[i]
        try:
            if parent[i] == i:
                return i
        except IndexError:
            letter = str(letter)

        return self.find(parent, parent[i])

    # A function that does union of two sets of x and y
    # (uses union by rank
    def union(self, parent, rank, x, y):
        """

        :param parent:
        :param rank:
        :param x:
        :param y:
        :return:
        """
        xroot = self.find(parent, x)
        yroot = self.find(parent, y)

        # Attach smaller rank tree under root of
        # high rank tree (Union by Rank)
        if rank[xroot] < rank[yroot]:
            parent[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            parent[yroot] = xroot

        # If ranks are same, then make one as root
        # and increment its rank by one
        else:
            parent[yroot] = xroot
            rank[xroot] += 1


class AdjacencyGraph:
    """

    """

    def __init__(self, vertices):
        """

        :param vertices:
        """
        self.V = vertices
        self._graph = [
            [0 for _ in range(vertices)] for _ in range(vertices)]

    # A utility function to find the vertex with
    # minimum distance value, from the set of vertices
    # not yet included in shortest path tree
    def minKey(self, key, mstSet):

        # Initilaize min value
        min_value = sys.maxsize
        min_index = None

        for v in range(self.V):
            if key[v] < min_value and mstSet[v] == False:
                min_value = key[v]
                min_index = v

        return min_index

    @property
    def matrix(self):
        """

        :return:
        """
        return self._graph

    @matrix.setter
    def matrix(self, g):
        """

        :return:
        """
        self._graph = g
