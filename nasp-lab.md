# 1. Zadatak

### Druga skupina zadataka

Za graf na slici programski pronaći najmanje razapinjajuće stablo (minimum spanning tree - MST)
pomoću sva tri algoritma obrađena na predavanjima (Kruskalov, Dijkstrin i Primov algoritam).
 - detekciju ciklusa riješiti po volji, a odabir i izvedba algoritma će svakako biti predmet razgovora
tijekom kolokviranja
 - rezultat (MST) treba prikladno ispisati ili vizualizirati

![graf](res/graph1.png)

# 2. Rješenje zadatka

## 2.1 Teorijski uvod

 - U teoriji grafova, najmanja razapinjajuća stabla predstavljaju stabla koja u sebi sadrže najmanji broj bridova kako bi se obišli svi vrhovi stabla
 - Kod težinskih grafova, dodatno se traži najjeftinija kombinacija bridova za obilazak svih vrhova
 - U ovome zadatku proučavaju se tri algoritma; **Kruskalov**, **Primov** i **Dijkstrin** algoritam.

## 2.2 Implementacija

 - Rješenje je implementirano u programskom jeziku Python 3.6. Dokumentacija i demonstracijski program generirani su pomoću Jupyter notebook alata.


```python
import graph
import graph.utils as utils

import sys

from string import ascii_lowercase
from importlib import reload
```

### 2.2.1. Kruskalov algoritam


```python
def kruskal(graph):
    result = []
    
    i = 0 # An index variable, used for sorted edges 
    e = 0 # An index variable, used for result[] 
    
    # Step 1:  Sort all the edges in non-decreasing  
    # order of their 
    # weight.  If we are not allowed to change the  
    # given graph, we can create a copy of graph 
    
    graph.edges = sorted(graph.edges, key=lambda item: item[2])
    
    parent = []
    rank = []
    
    for node in range(graph.V):
        parent.append(node)
        rank.append(0)
        
    while e < graph.V - 1:
        # Step 2: Pick the smallest edge and increment  
        # the index for next iteration 
        u,v,w =  graph.edges[i] 
        i = i + 1
        x = graph.find(parent, u) 
        y = graph.find(parent ,v) 
        
        # If including this edge does't cause cycle,  
        # include it in result and increment the index 
        # of result for next edge 
        
        if x != y: 
            e = e + 1     
            result.append([u,v,w]) 
            graph.union(parent, rank, x, y)             
        # Else discard the edge
        
    return result
```


```python
graph1 = utils.create_lab_graph()
graph1.edges = utils.letters_to_indices(graph1.edges)
```


```python
mst = kruskal(graph1)
mst = utils.indices_to_letters(mst)
utils.print_tree(mst)
```

    Following are the edges in the constructed MST
    Edge 	Weight
    c - d 	 1
    e - h 	 1
    f - n 	 1
    n - r 	 1
    a - b 	 2
    b - d 	 2
    d - h 	 2
    f - j 	 2
    h - m 	 2
    i - m 	 2
    m - r 	 2
    s - v 	 2
    g - l 	 3
    o - v 	 3
    g - h 	 4
    m - s 	 4
    r - t 	 4
    v - w 	 4
    u - x 	 5
    r - u 	 6
    g - k 	 7
    l - p 	 7
    v - y 	 8


### 2.2.2. Primov Algoritam


```python
def prim(graph):
    # Key values used to pick minimum weight edge in cut
    key = [sys.maxsize] * graph.V
    parent = [None] * graph.V  # Array to store constructed MST

    # Make key 0 so that this vertex is picked as first vertex
    key[0] = 0
    mstSet = [False] * graph.V

    parent[0] = -1  # First node is always the root of

    for cout in range(graph.V):
        # Pick the minimum distance vertex from
        # the set of vertices not yet processed.
        # u is always equal to src in first iteration
        u = graph.minKey(key, mstSet)

        # Put the minimum distance vertex in
        # the shortest path tree
        mstSet[u] = True

        # Update dist value of the adjacent vertices
        # of the picked vertex only if the current
        # distance is greater than new distance and
        # the vertex in not in the shotest path tree
        for v in range(graph.V):
            # graph[u][v] is non zero only for adjacent vertices of m
            # mstSet[v] is false for vertices not yet included in MST
            # Update the key only if graph[u][v] is smaller than key[v]
            if graph.matrix[u][v] > 0 and mstSet[v] == False and key[v] > \
                    graph.matrix[u][v]:
                key[v] = graph.matrix[u][v]
                parent[v] = u

    return parent
```


```python
g = utils.create_lab_graph()
g_adj = utils.create_adjacency_matrix_graph(g)

tree = prim(g_adj)
utils.print_tree_adj(tree, g_adj)
```

    Following are the edges in the constructed MST
    Edge 	Weight
    a - b 	 2
    d - c 	 1
    b - d 	 2
    h - e 	 1
    n - f 	 1
    h - g 	 4
    d - h 	 2
    m - i 	 2
    f - j 	 2
    g - k 	 7
    g - l 	 3
    h - m 	 2
    r - n 	 1
    v - o 	 3
    l - p 	 7
    m - r 	 2
    m - s 	 4
    r - t 	 4
    r - u 	 6
    s - v 	 2
    v - w 	 4
    u - x 	 5
    v - y 	 8


### 2.2.3. Dijkstrin Algoritam


```python
def dijkstra(graph):
    tree = []
    parent = []
    rank = []

    for node in range(graph.V):
        parent.append(node)
        rank.append(0)

    for u, v, w in graph.edges:
        tree.append([u, v, w])

        x = graph.find(parent, u)
        y = graph.find(parent, v)

        if x == y:
            tree.remove(max(tree, key=lambda item: item[2]))
        else:
            graph.union(parent, rank, x, y)

    return tree
```


```python
g = utils.create_lab_graph()
g.edges = utils.letters_to_indices(g.edges)

tree = dijkstra(g)
tree = utils.indices_to_letters(tree)
utils.print_tree(tree)
```

    Following are the edges in the constructed MST
    Edge 	Weight
    a - b 	 2
    b - d 	 2
    c - d 	 1
    c - h 	 3
    d - h 	 2
    e - h 	 1
    f - j 	 2
    f - n 	 1
    g - h 	 4
    g - l 	 3
    h - i 	 3
    h - m 	 2
    i - m 	 2
    j - o 	 5
    m - s 	 4
    m - r 	 2
    n - r 	 1
    o - v 	 3
    r - t 	 4
    s - v 	 2
    u - x 	 5
    v - y 	 8
    v - w 	 4


### 2.2.4. Rezultirajuće najmanje razapinjajuće stablo

![mst](res/graph2.png)

# 3. Zaključak

 - Svaki od algoritama ima svoje prednosti i mane
 - Nedostakak Kruskalovog algoritma je potreba za sortiranjem bridova, što naravno ovisi o samom sortiranju i detekciji ciklusa
 - Prednost Dijkstrinog algoritma nad Kruskalovim je to što nema potrebe za sortiranjem.
 
 - Složenost Kruskalovog algoritma: **O(E * log2(V))**
 - Složenost Dijsktrinog algoritma: **O(E * V)**
 - Složenost Primovog algoritma: **O(E + V * log2(V))**

# 4. Literatura

 - *Napredni Algoritmi i Strukture Podataka, predavanja 2018./2019.*
