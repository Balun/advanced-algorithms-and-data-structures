"""

"""

import sys


def kruskal(graph):
    """

    :param graph:
    :return:
    """
    result = []  # This will store the resultant MST

    i = 0  # An index variable, used for sorted edges
    e = 0  # An index variable, used for result[]

    # Step 1:  Sort all the edges in non-decreasing
    # order of their
    # weight.  If we are not allowed to change the
    # given graph, we can create a copy of graph
    graph.edges = sorted(graph.edges, key=lambda item: item[2])

    parent = [];
    rank = []

    # Create V subsets with single elements
    for node in range(graph.V):
        parent.append(node)
        rank.append(0)

        # Number of edges to be taken is equal to V-1
    while e < graph.V - 1:

        # Step 2: Pick the smallest edge and increment
        # the index for next iteration
        u, v, w = graph.edges[i]
        i = i + 1
        x = graph.find(parent, u)
        y = graph.find(parent, v)

        # If including this edge does't cause cycle,
        # include it in result and increment the index
        # of result for next edge
        if x != y:
            e = e + 1
            result.append([u, v, w])
            graph.union(parent, rank, x, y)
            # Else discard the edge

    return result


def prim(graph):
    """

    :param graph:
    :return:
    """
    # Key values used to pick minimum weight edge in cut
    key = [sys.maxsize] * graph.V
    parent = [None] * graph.V  # Array to store constructed MST

    # Make key 0 so that this vertex is picked as first vertex
    key[0] = 0
    mstSet = [False] * graph.V

    parent[0] = -1  # First node is always the root of

    for cout in range(graph.V):
        # Pick the minimum distance vertex from
        # the set of vertices not yet processed.
        # u is always equal to src in first iteration
        u = graph.minKey(key, mstSet)

        # Put the minimum distance vertex in
        # the shortest path tree
        mstSet[u] = True

        # Update dist value of the adjacent vertices
        # of the picked vertex only if the current
        # distance is greater than new distance and
        # the vertex in not in the shotest path tree
        for v in range(graph.V):
            # graph[u][v] is non zero only for adjacent vertices of m
            # mstSet[v] is false for vertices not yet included in MST
            # Update the key only if graph[u][v] is smaller than key[v]
            if graph.matrix[u][v] > 0 and mstSet[v] == False and key[v] > \
                    graph.matrix[u][v]:
                key[v] = graph.matrix[u][v]
                parent[v] = u

    return parent


def dijkstra(graph):
    """

    :param graph:
    :return:
    """
    tree = []
    parent = []
    rank = []

    for node in range(graph.V):
        parent.append(node)
        rank.append(0)

    for u, v, w in graph.edges:
        tree.append([u, v, w])

        x = graph.find(parent, u)
        y = graph.find(parent, v)

        if x == y:
            tree.remove(max(tree, key=lambda item: item[2]))
        else:
            graph.union(parent, rank, x, y)

    return tree
