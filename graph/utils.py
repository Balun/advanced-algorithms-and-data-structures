import graph

from string import ascii_lowercase

cutom_letters = ascii_lowercase.replace('q', '')


def create_lab_graph():
    """

    :return:
    """
    g = graph.Graph(24)

    g.add_edge('a', 'b', 2)
    g.add_edge('a', 'c', 4)
    g.add_edge('b', 'c', 5)
    g.add_edge('b', 'd', 2)
    g.add_edge('b', 'e', 7)
    g.add_edge('c', 'd', 1)
    g.add_edge('c', 'g', 8)
    g.add_edge('c', 'h', 3)
    g.add_edge('d', 'g', 7)
    g.add_edge('d', 'h', 2)
    g.add_edge('e', 'h', 1)
    g.add_edge('e', 'f', 8)
    g.add_edge('f', 'i', 4)
    g.add_edge('f', 'j', 2)
    g.add_edge('f', 'n', 1)
    g.add_edge('g', 'h', 4)
    g.add_edge('g', 'k', 7)
    g.add_edge('g', 'l', 3)
    g.add_edge('h', 'i', 3)
    g.add_edge('h', 'm', 2)
    g.add_edge('h', 'l', 7)
    g.add_edge('i', 'm', 2)
    g.add_edge('i', 'n', 6)
    g.add_edge('j', 'o', 5)
    g.add_edge('k', 'p', 14)
    g.add_edge('l', 'p', 7)
    g.add_edge('l', 't', 8)
    g.add_edge('m', 'n', 9)
    g.add_edge('m', 's', 4)
    g.add_edge('m', 'r', 2)
    g.add_edge('n', 'o', 9)
    g.add_edge('n', 'r', 1)
    g.add_edge('n', 'v', 10)
    g.add_edge('o', 'v', 3)
    g.add_edge('r', 't', 4)
    g.add_edge('r', 'u', 6)
    g.add_edge('s', 'v', 2)
    g.add_edge('u', 'v', 11)
    g.add_edge('u', 'x', 5)
    g.add_edge('v', 'x', 6)
    g.add_edge('v', 'y', 8)
    g.add_edge('v', 'w', 4)
    g.add_edge('w', 'y', 9)

    return g


def create_adjacency_matrix_graph(g1):
    """

    :return:
    """
    g = graph.AdjacencyGraph(24)

    matrix = [[0 for column in range(g.V)] for row in range(g.V)]

    for u, v, w in g1.edges:
        matrix[cutom_letters.index(u)][cutom_letters.index(v)] = w
        matrix[cutom_letters.index(v)][cutom_letters.index(u)] = w

    g.matrix = matrix
    return g


def letters_to_indices(edges):
    """

    :param graph:
    :return:
    """
    new_edges = [[cutom_letters.index(u), cutom_letters.index(v), w] for u,
                                                                         v,
                                                                         w
                 in edges]

    return new_edges


def indices_to_letters(edges):
    """

    :param graph:
    :return:
    """
    new_edges = [[cutom_letters[u], cutom_letters[v], w] for u,
                                                             v,
                                                             w
                 in edges]

    return new_edges


def print_tree(tree):
    """

    :param tree:
    :return:
    """
    print("Following are the edges in the constructed MST")
    print("Edge \tWeight")

    for u, v, w in tree:
        print(u, "-", v, "\t", w)


def print_tree_adj(parent, graph):
    """

    :param parent:
    :param adj_matrix:
    :return:
    """
    print("Following are the edges in the constructed MST")
    print("Edge \tWeight")
    for i in range(1, graph.V):
        print(cutom_letters[parent[i]], "-", cutom_letters[i], "\t",
              graph.matrix[i][
            parent[i]])
